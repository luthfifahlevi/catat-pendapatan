package com.bot.mymoney.handler.expense;

/** Pengeluaran Handler. **/
public abstract class PengeluaranHandler {
  protected PengeluaranHandler handler;
  protected String description;
  protected String messageToUser;

  /** 'else' for verificationMessage. **/
  public PengeluaranHandler cancelOperation(String message) {
    if (message.equalsIgnoreCase("batal")) {
      messageToUser = "Proses fitur dibatalkan";
      return null;
    }
    return unknownMessage();
  }

  public String getMessageToUser() {
    return messageToUser;
  }

  public abstract PengeluaranHandler unknownMessage();

  public abstract PengeluaranHandler verificationMessage(String message);

  public abstract PengeluaranHandler handle(String message);

  public abstract String getDescription();
}
