package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;
import java.util.HashMap;
import java.util.Map;

/** InvestmentCalculator. **/
public abstract class InvestmentCalculator extends ResponseTemplate implements Handler {
  protected String mode;
  protected Map<String, Integer> vars = new HashMap<String, Integer>() {{
      put("funds", 100000);
      put("time", 10);
      put("rate", 8);
      put("contrib", 30000);
    }};
  protected Integer contrib;
  protected Integer result;

  @Override
  public ResponseTemplate verificationMessage(String message) {
    if (message.equalsIgnoreCase("ya")) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public String getDescription() {
    description = "Kalkulator yang sedang berjalan adalah kalkulator " + mode;
    return this.description;
  }

  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Tidak terdapat opsi tersebut! "
        + "Silakan pilih kembali opsi yang tersedia "
        + "Jika ingin membatalkan tindakan, ketik 'Batal'";
    return this;
  }

  public void input(String variable, Integer value) {
    vars.put(variable, value);
  }

  protected abstract void calculateInvestment();

  public abstract String response();

  /** getValues. **/
  public String getValues() {
    return "Saldo: " + vars.get("funds")
        + ", Tahun: " + vars.get("time")
        + ", Pengembalian: " + vars.get("rate")
        + ", Kontribusi (" + mode + "): " + vars.get("contrib");
  }

  Integer getInvestmentGrowth() {
    Integer current = vars.get("funds");
    int rate = vars.get("rate");
    for (int i = 0; i < vars.get("time"); i++) {
      int interest = current * rate / 100;
      current += vars.get("contrib") + interest;
    }
    return current;
  }
}
