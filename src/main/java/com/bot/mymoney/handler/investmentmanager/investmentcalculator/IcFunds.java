package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.ResponseTemplate;

/** ICFunds. **/
public class IcFunds extends CalculatorInputHandler {
  public IcFunds(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("funds", value);
    //TODO: Message
    return new IcTime(calculator);
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
