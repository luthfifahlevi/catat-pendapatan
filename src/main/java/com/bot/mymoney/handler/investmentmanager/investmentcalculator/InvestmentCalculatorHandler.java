package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;
import java.util.Arrays;
import java.util.List;

/**
 * InvestmentCalculatorHandler.
 **/
public class InvestmentCalculatorHandler extends ResponseTemplate implements Handler {
  final List<String> options = Arrays.asList("Bulanan", "Tahunan");

  @Override
  public ResponseTemplate verificationMessage(String message) {
    if (options.contains(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public ResponseTemplate handle(String message) {
    description = message;
    messageToUser = "Oke, opsi yang dipilih adalah Kalkulator " + message
        + ". Ketik 'Ya' untuk lanjut lalu mohon ditunggu sebentar.";
    if (message.equalsIgnoreCase(options.get(0))) {
      return new CalculatorMonthlyStart();
    }
    if (message.equalsIgnoreCase(options.get(1))) {
      return new CalculatorAnnuallyStart();
    }
    return cancelOperation(message);
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }


  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Tidak terdapat opsi tersebut! "
        + "Silakan pilih kembali opsi yang tersedia "
        + "Jika ingin membatalkan tindakan, ketik 'Batal'";
    return this;
  }
}
