package com.bot.mymoney.handler.investmentmanager;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;
import com.bot.mymoney.handler.investmentmanager.investmentcalculator.InvestmentCalculatorHandler;
import java.util.Arrays;
import java.util.List;

/** InvestmentManagerHandler. **/
public class InvestmentManagerHandler extends ResponseTemplate implements Handler {
  final List<String> options = Arrays.asList("Kalkulator");

  @Override
  public ResponseTemplate verificationMessage(String message) {
    if (options.contains(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public ResponseTemplate handle(String message) {
    if (message.equalsIgnoreCase(options.get(0))) {
      description = message;
      messageToUser = "Oke, opsi yang dipilih adalah " + message + ". Mohon tunggu sebentar.";
      return new InvestmentCalculatorHandler();
    }
    return null;
  }

  @Override
  public String getDescription() {
    return this.description;
  }

  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Tidak terdapat opsi tersebut! "
        + "Silakan pilih kembali opsi yang tersedia "
        + "Jika ingin membatalkan tindakan, ketik 'Batal'";
    return this;
  }
}
