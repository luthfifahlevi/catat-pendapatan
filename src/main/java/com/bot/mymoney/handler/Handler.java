package com.bot.mymoney.handler;

/** Handler for every nextHandler. **/
public interface Handler {
  ResponseTemplate verificationMessage(String message);

  ResponseTemplate handle(String message);

  String getDescription();
}
