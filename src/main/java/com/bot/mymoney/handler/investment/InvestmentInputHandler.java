package com.bot.mymoney.handler.investment;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;

/** InvestmentInputHandler. **/
public abstract class InvestmentInputHandler  extends ResponseTemplate implements Handler {
  protected InvestmentCalculator calculator;

  public InvestmentInputHandler(InvestmentCalculator calculator) {
    this.calculator = calculator;
  }

  @Override
  public ResponseTemplate verificationMessage(String message) {
    try {
      return handle(message);
    } catch (NumberFormatException e) {
      return cancelOperation(message);
    }
  }

  @Override
  public ResponseTemplate unknownMessage() {
    return null;
  }
}
