package com.bot.mymoney.handler.investment.investmentcalcluatormonthly;

import com.bot.mymoney.handler.ResponseTemplate;
import com.bot.mymoney.handler.investment.InvestmentCalculator;
import com.bot.mymoney.handler.investment.InvestmentInputHandler;

/** ICMTime. **/
public class IcmTime extends InvestmentInputHandler {
  public IcmTime(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("time", value);
    //TODO: Message
    return new IcmRate(calculator);
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
