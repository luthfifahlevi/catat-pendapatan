package com.bot.mymoney.handler.investment.investmentcalcluatormonthly;

import com.bot.mymoney.handler.ResponseTemplate;
import com.bot.mymoney.handler.investment.InvestmentCalculator;
import com.bot.mymoney.handler.investment.InvestmentInputHandler;

/** ICMConfirm. **/
public class IcmConfrim extends InvestmentInputHandler {
  public IcmConfrim(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    if ("ya".equalsIgnoreCase(message)) {
      description = message;
      messageToUser = calculator.response();
      return null;
    } else {
      throw new NumberFormatException();
    }
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
