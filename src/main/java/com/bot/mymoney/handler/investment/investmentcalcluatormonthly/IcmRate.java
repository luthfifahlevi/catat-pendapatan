package com.bot.mymoney.handler.investment.investmentcalcluatormonthly;

import com.bot.mymoney.handler.ResponseTemplate;
import com.bot.mymoney.handler.investment.InvestmentCalculator;
import com.bot.mymoney.handler.investment.InvestmentInputHandler;

/** ICMRate. **/
public class IcmRate extends InvestmentInputHandler {
  public IcmRate(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("rate", value);
    //TODO: Message
    return new IcmContrib(calculator);
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
