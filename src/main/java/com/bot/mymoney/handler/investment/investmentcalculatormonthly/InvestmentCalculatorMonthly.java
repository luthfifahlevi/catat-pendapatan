package com.bot.mymoney.handler.investment.investmentcalculatormonthly;

import com.bot.mymoney.handler.ResponseTemplate;
import com.bot.mymoney.handler.investment.InvestmentCalculator;

/** InvestmentCalculatorMonthly. **/
public class InvestmentCalculatorMonthly extends InvestmentCalculator {
  public InvestmentCalculatorMonthly() {
    mode = "Awal Bulan";
  }

  @Override
  public ResponseTemplate handle(String message) {
    messageToUser = "Berapa saldo awal kamu? Contoh: 100000";
    return new IcmFunds(this);
  }

  @Override
  protected void calculateInvestment() {
    contrib = vars.get("contrib") * vars.get("time");
    Integer current = vars.get("funds");
    int rate = vars.get("rate");
    for (int i = 0; i < vars.get("time"); i++) {
      int interest = current * rate / 100;
      current += vars.get("contrib") + interest;
    }
    result = current;
  }

  @Override
  public String response() {
    calculateInvestment();
    return "Dalam jangka waktu " + vars.get("time")
        + " tahun, saldo akhirmu akan menjadi Rp" + result
        + " dengan total kontribusi Rp" + contrib + ".";
  }
}
