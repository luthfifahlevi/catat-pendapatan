package com.bot.mymoney.handler.investment.investmentcalculatormonthly;

import com.bot.mymoney.handler.ResponseTemplate;
import com.bot.mymoney.handler.investment.InvestmentCalculator;
import com.bot.mymoney.handler.investment.InvestmentInputHandler;

/** Confirmation. **/
public class IcmConfirm extends InvestmentInputHandler {
  public IcmConfirm(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    if ("ya".equalsIgnoreCase(message)) {
      description = message;
      messageToUser = calculator.response();
      return null;
    } else {
      throw new NumberFormatException();
    }
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
