package com.bot.mymoney.handler.investment.investmentcalcluatormonthly;

import com.bot.mymoney.handler.ResponseTemplate;
import com.bot.mymoney.handler.investment.InvestmentCalculator;
import com.bot.mymoney.handler.investment.InvestmentInputHandler;

/** ICMContrib. **/
public class IcmContrib extends InvestmentInputHandler {
  public IcmContrib(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("rate", value);
    description = message;
    messageToUser = "Apakah benar input anda:"
        + calculator.getValues()
        + "? Ketik 'ya' untuk lanjut.";
    return new IcmConfrim(calculator);
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
