package com.bot.mymoney.handler.investment.investmentcalculatormonthly;

import com.bot.mymoney.handler.ResponseTemplate;
import com.bot.mymoney.handler.investment.InvestmentCalculator;
import com.bot.mymoney.handler.investment.InvestmentInputHandler;

/** ICMFunds. **/
public class IcmFunds extends InvestmentInputHandler {
  public IcmFunds(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("funds", value);
    //TODO: Message
    return new IcmTime(calculator);
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
