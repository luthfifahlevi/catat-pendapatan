package com.bot.mymoney.handler.record;

/** PendapatanTemplate. **/
public abstract class PendapatanTemplate {
  protected PendapatanHandler handler;
  protected String description;
  protected String messageToUser;

  public abstract PendapatanTemplate unknownMessage();

  /** 'else' for verificationMessage. **/
  public PendapatanTemplate cancelOperation(String message) {
    if (message.equalsIgnoreCase("batal")) {
      messageToUser = "Proses fitur dibatalkan";
      return null;
    }
    return unknownMessage();
  }

  public String getMessageToUser() {
    return messageToUser;
  }
}
