package com.bot.mymoney.handler.record;

/** Pendapatan Handler. **/
public interface PendapatanHandler {
  PendapatanTemplate verificationMessage(String message);

  PendapatanTemplate handle(String message);

  String getDescription();
}
