package com.bot.mymoney.handler.laporan;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;
import java.util.Arrays;
import java.util.List;


/** LaporanWaktuHandler untuk memilih waktu dan mengeluarkan output nominal berdasarkan waktu. **/
public class LaporanWaktuHandler extends ResponseTemplate implements Handler {
  public static final List<String> kategoriWaktu = Arrays.asList("harian", "mingguan", "bulanan");
  LaporanCatatanHandler catatan;

  //  @Autowired
  //  RecordDb recordDb;

  //  @Autowired
  //  ExpenseDb expenseDb;

  public LaporanWaktuHandler(LaporanCatatanHandler catatan) {
    this.catatan = catatan;
    this.description = "";
  }

  @Override
  public ResponseTemplate verificationMessage(String message) {
    if (kategoriWaktu.contains(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public ResponseTemplate handle(String message) {
    description = catatan.getDescription() +  ";" + message;
    int days = this.stringToDay(message);
    String catat = getDescription().split(";") [2];
    messageToUser = categoryCatatan(catat, days);
    return null;
  }

  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Kategori yang dipilih tidak ada, tolong pilih salah satu dari "
      + "kategori yang tersedia. Ketik 'Batal' jika ingin membatalkan.";
    return this;
  }

  @Override
  public String getDescription() {
    return catatan.getDescription();
  }

  private String categoryCatatan(String catat, int days) {
    if (catat.equalsIgnoreCase("pengeluaran")) {
      return totalNominalExpense(days);
    }
    return totalNominalRecords(days);
  }

  private String totalNominalExpense(int hari) {
    int totalNominal = 0;
    //    String userId = getDescription().split(";") [0];
    //    List<Expense> listOfExpense = expenseDb.getByUserId(userId);
    //    LocalDateTime dateNow = LocalDateTime.now().plusHours(7);
    //    for (Expense expense : listOfExpense) {
    //      LocalDateTime date = LocalDateTime.parse(expense.getTimestamp(),
    //          DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
    //      if (Duration.between(dateNow, date).toDays() < hari) {
    //        totalNominal += Integer.valueOf(expense.getNominal());
    //      }
    //    }
    return "Jumlah Pengeluaran Total = " + totalNominal;
  }

  private String totalNominalRecords(int hari) {
    int totalNominal = 0;
    //    String userId = getDescription().split(";") [0];
    //    List<Record> listOfRecord = recordDb.getByUserId(userId);
    //    LocalDateTime dateNow = LocalDateTime.now().plusHours(7);
    //    for (Record record : listOfRecord) {
    //      LocalDateTime date = LocalDateTime.parse(record.getTimestamp(),
    //          DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
    //      if (Duration.between(dateNow, date).toDays() < hari) {
    //        totalNominal += Integer.valueOf(record.getNominal());
    //      }
    //    }
    return "Jumlah Pendapatan Total = " + totalNominal;
  }

  private int stringToDay(String message) {
    if (message.equalsIgnoreCase("Harian")) {
      return 1;
    } else if (message.equalsIgnoreCase("Mingguan")) {
      return 7;
    } else {
      return 30;
    }
  }

}


