package com.bot.mymoney.handler.laporan;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;
import java.util.Arrays;
import java.util.List;

/** Implementasi laporan catatan untuk memilih catatan. **/
public class LaporanCatatanHandler extends ResponseTemplate implements Handler {

  public static final List<String> kategoriCatatan = Arrays.asList(
      "laporan pengeluaran",
      "laporan pendapatan"
  );

  public LaporanCatatanHandler(String userId, String name) {
    this.description = userId + ";" + name;
  }

  @Override
  public ResponseTemplate verificationMessage(String message) {
    if (kategoriCatatan.contains(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public ResponseTemplate handle(String message) {
    description += ";" + message;
    messageToUser = "Berapa jangka waktu laporan " + message
      + " yang ingin kamu pilih? Jawab dengan 'harian', 'mingguan', atau 'bulanan'.";
    return new LaporanWaktuHandler(this);
  }

  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Kategori yang dipilih tidak ada, tolong pilih salah satu dari "
        + "kategori yang tersedia. Ketik 'Batal' jika ingin membatalkan.";
    return this;
  }

  @Override
  public String getDescription() {
    return this.description;
  }
}
