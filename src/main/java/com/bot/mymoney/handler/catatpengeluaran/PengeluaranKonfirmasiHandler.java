package com.bot.mymoney.handler.catatpengeluaran;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;

/**
 * Handler untuk konfirmasi pencatatan catat pengeluaran.
 **/
public class PengeluaranKonfirmasiHandler extends ResponseTemplate implements Handler {
  public PengeluaranKonfirmasiHandler(PengeluaranNominalHandler state) {
    this.state = state;
    this.description = "";
  }

  @Override
  public ResponseTemplate verificationMessage(String message) {
    if (message.equalsIgnoreCase("ya")) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public ResponseTemplate handle(String message) {
    messageToUser = "Pengeluaran kamu berhasil dicatat!";
    description = "";
    return null;
  }

  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Untuk konfirmasi pencatatan jawab 'Ya'"
            + " dan untuk pembatalan pencatatan jawab 'Batal'.";
    return this;
  }

  @Override
  public String getDescription() {
    return state.getDescription();
  }
}
