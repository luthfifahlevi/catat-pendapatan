package com.bot.mymoney.handler.catatpengeluaran;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;

/**
 * Handler untuk input nominal catat pengeluaran.
 **/
public class PengeluaranNominalHandler extends ResponseTemplate implements Handler {
  public PengeluaranNominalHandler(PengeluaranKategoriHandler state) {
    this.state = state;
    this.description = "";
  }

  @Override
  public ResponseTemplate verificationMessage(String message) {
    if (isNominal(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public ResponseTemplate handle(String message) {
    description = message;
    messageToUser = "Oke, uang yang digunakan sebesar Rp" + message
            + ". Konfirmasi pencatatan dengan menjawab 'Ya' "
            + "atau ketik 'Batal' untuk membatalkan tindakan";
    return new PengeluaranKonfirmasiHandler(this);
  }

  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Nominal uang tidak sesuai. "
            + "Pastikan kamu hanya memasukkan angka, contoh: 50000. "
            + "Jika ingin membatalkan tindakan, ketik 'Batal'";
    return this;
  }

  @Override
  public String getDescription() {
    return state.getDescription() + ";" + this.description;
  }

  private boolean isNominal(String userMessage) {
    try {
      return Integer.parseInt(userMessage) > 0;
    } catch (NumberFormatException e) {
      return false;
    }
  }
}
