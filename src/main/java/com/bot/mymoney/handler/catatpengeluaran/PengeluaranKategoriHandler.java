package com.bot.mymoney.handler.catatpengeluaran;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;
import java.util.ArrayList;
import java.util.List;

/**
 * Handler untuk pemilihan kategori catat pengeluaran.
 **/
public class PengeluaranKategoriHandler extends ResponseTemplate implements Handler {
  private static final List<String> kategoriPengeluaran = new ArrayList<String>() {
    {
      add("konsumsi");
      add("transportasi");
      add("utilitas");
      add("belanja");
      add("lainnya");
    }
  };

  public PengeluaranKategoriHandler(String userId, String name) {
    this.description = userId + ";" + name;
  }

  @Override
  public ResponseTemplate verificationMessage(String message) {
    if (kategoriPengeluaran.contains(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public ResponseTemplate handle(String message) {
    description += ";" + message;
    messageToUser = "Kategori " + message + " berhasil terpilih."
            + " Berapa jumlah uang yang ingin kamu masukkan ke catatan pengeluaran?"
            + " Contoh: 50000";
    return new PengeluaranNominalHandler(this);
  }

  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Kategori yang dipilih tidak ada"
            + ", tolong pilih salah satu dari kategori yang tersedia."
            + " Ketik 'Batal' jika ingin membatalkan.";
    return this;
  }

  @Override
  public String getDescription() {
    return this.description;
  }
}
