package com.bot.mymoney.handler.catatpendapatan;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;
import java.util.Arrays;
import java.util.List;

/** KategoriHandler. **/
public class PendapatanKategoriHandler extends ResponseTemplate implements Handler {
  final List<String> categories = Arrays.asList("gaji", "usaha", "lainnya");

  public PendapatanKategoriHandler(String senderId, String name) {
    this.description = senderId + ";" + name;
  }

  @Override
  public ResponseTemplate verificationMessage(String message) {
    if (categories.contains(message)) {
      return handle(message);
    }
    return cancelOperation(message);
  }

  @Override
  public ResponseTemplate handle(String message) {
    description += ";" + message;
    messageToUser = "Kategori " + message + " berhasil dipilih. "
        + "Berapa pendapatan kamu? \nContoh: 8000000";
    return new PendapatanNominalHandler(this);
  }

  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Tidak terdapat kategori tersebut! "
        + "Silakan pilih kembali kategori yang tersedia "
        + "Jika ingin membatalkan tindakan, ketik 'Batal'";
    return this;
  }

  @Override
  public String getDescription() {
    return this.description;
  }
}
