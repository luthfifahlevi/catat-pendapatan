package com.bot.mymoney.handler.catatpendapatan;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;

/** NominalHandler. **/
public class PendapatanNominalHandler extends ResponseTemplate implements Handler {
  public PendapatanNominalHandler(PendapatanKategoriHandler state) {
    this.state = state;
  }

  @Override
  public ResponseTemplate verificationMessage(String message) {
    try {
      return handle(message);
    } catch (NumberFormatException e) {
      return cancelOperation(message);
    }
  }

  @Override
  public ResponseTemplate handle(String message) {
    Integer.parseInt(message);
    description = message;
    messageToUser = "Apakah benar pendapatan kamu sebesar Rp" + message
        + "? Konfirmasi pencatatan dengan menjawab 'Ya' "
        + "atau ketik 'Batal' untuk membatalkan tindakan";
    return new PendapatanKonfirmasiHandler(this);
  }

  @Override
  public ResponseTemplate unknownMessage() {
    messageToUser = "Nominal uang tidak sesuai. "
        + "Pastikan kamu hanya memasukkan angka, contoh: 50000. "
        + "Jika ingin membatalkan tindakan, ketik 'Batal'";
    return this;
  }

  @Override
  public String getDescription() {
    return state.getDescription() + ";" + description;
  }
}
