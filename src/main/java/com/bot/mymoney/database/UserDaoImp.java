package com.bot.mymoney.database;

import com.bot.mymoney.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

/** UserDaoImplementation. **/
public class UserDaoImp implements UserDao {
  private JdbcTemplate jdbcTemplate;
  private static final String USER_TABLE = "tbl_user";
  private static final String SQL_SELECT_ALL = "SELECT * FROM " + USER_TABLE;
  private static final ResultSetExtractor<List<User>>
      USER_EXTRACTOR = UserDaoImp::extractData;

  public UserDaoImp(DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);
  }

  /**   extract data user table.   **/
  public static List<User> extractData(ResultSet resultSet) throws SQLException {
    List<User> extractList = new ArrayList<User>();
    while (resultSet.next()) {
      User p = new User(
          resultSet.getString("user_id"),
          resultSet.getString("name"),
          resultSet.getString("remind"));
      extractList.add(p);
    }
    return extractList;
  }

  /**   function for user database.   **/
  @Override
  public int registerUser(String userId, String name) {
    String register = "INSERT INTO " + USER_TABLE
        + " (user_id, name, remind) VALUES (?, ?, ?);";
    return jdbcTemplate.update(register, userId, name, "false");
  }

  @Override
  public List<User> get() {
    return jdbcTemplate.query(SQL_SELECT_ALL, USER_EXTRACTOR);
  }

  @Override
  public List<User> getByUserId(String userId) {
    String sqlGetByUserId = SQL_SELECT_ALL + " WHERE LOWER(user_id) LIKE LOWER(?);";
    return jdbcTemplate.query(sqlGetByUserId, new Object[]{"%" + userId + "%"},
        USER_EXTRACTOR);
  }

  @Override
  public List<User> getStatusNotifikasiByUserId(String userId) {
    String sqlGetStatusNotifikasi = SQL_SELECT_ALL + " WHERE LOWER(user_id) LIKE LOWER(?);";
    return jdbcTemplate.query(sqlGetStatusNotifikasi, new Object[]{"%" + userId + "%"},
        USER_EXTRACTOR);
  }

  @Override
  public int setStatusNotifikasiByUserId(String status, String userId) {
    String sqlUpdateStatusNotifikasi = "UPDATE " + USER_TABLE
        + " SET remind = ? WHERE LOWER(user_id) LIKE LOWER(?);";
    return jdbcTemplate.update(sqlUpdateStatusNotifikasi, status, "%" + userId + "%");
  }

  @Override
  public List<User> getAllUserNotifikasiAktif() {
    String sqlGetAllNotifikasiTrue = SQL_SELECT_ALL + " WHERE remind='true';";
    return jdbcTemplate.query(sqlGetAllNotifikasiTrue, USER_EXTRACTOR);
  }
}
