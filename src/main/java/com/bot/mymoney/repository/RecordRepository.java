package com.bot.mymoney.repository;

import com.bot.mymoney.database.RecordDao;
import com.bot.mymoney.model.Expense;
import com.bot.mymoney.model.Record;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/** RecordDb. **/
@Repository
public class RecordRepository {

  @Autowired
  private RecordDao recordDao;

  /** Input user record to database. **/
  public int saveRecord(String description) {
    String[] userData = description.split(";");
    LocalDateTime localDateTime = LocalDateTime.now();
    localDateTime = localDateTime.plusHours(7);
    return recordDao.saveRecord(userData[0], userData[1],
        userData[2], String.valueOf(localDateTime), userData[3]);
  }

  public List<Record> getAll() {
    return recordDao.get();
  }

  public List<Record> getByUserId(String userId) {
    return recordDao.getByUserId(userId);
  }
}