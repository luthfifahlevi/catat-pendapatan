package com.bot.mymoney.repository;

import com.bot.mymoney.database.ExpenseDao;
import com.bot.mymoney.model.Expense;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Database Repository for Expense.
 **/
@Repository
public class ExpenseDb {

  @Autowired
  private ExpenseDao expenseDao;

  /**
   * Saving Expense.
   **/
  public int saveExpense(String description) {
    String[] data = description.split(";");
    LocalDateTime timestamp = LocalDateTime.now().plusHours(7);
    return expenseDao.saveExpense(
        data[0],
        data[1],
        data[2],
        String.valueOf(timestamp),
        data[3]
    );
  }

  public List<Expense> getByUserId(String userId) {
    return expenseDao.getByUserId(userId);
  }
}
