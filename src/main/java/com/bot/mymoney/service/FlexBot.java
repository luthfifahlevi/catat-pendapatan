package com.bot.mymoney.service;

import com.linecorp.bot.model.message.FlexMessage;

/** FlexMessage for user. **/
public interface FlexBot {

  FlexMessage createFlexMenu();

  FlexMessage createFlexCatatPendapatan();

  FlexMessage createFlexCatatPengeluaran();

  FlexMessage createFlexLaporan();

  FlexMessage createFlexLaporanWaktu();
}
