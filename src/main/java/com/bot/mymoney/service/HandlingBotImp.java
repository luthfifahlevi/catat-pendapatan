package com.bot.mymoney.service;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.profile.UserProfileResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/** Handling Message Bot Implementation. **/
@Service
public class HandlingBotImp implements HandlingBot {
  private Source source;
  @Autowired
  private FlexBot flexBot;
  @Autowired
  private ResponseBot responseBot;

  private String replyToken;
  private String userMessage;
  private String senderId;
  private UserProfileResponse profileUser;

  private final String url = "https://dumbmoney.herokuapp.com/";

  private HashMap<String, String> currentPath = new HashMap<>();

  /** Handling Message from user. **/
  public void handleMessageEvent(MessageEvent messageEvent) {
    TextMessageContent textMessageContent = (TextMessageContent) messageEvent.getMessage();
    String textMessage = textMessageContent.getText();
    userMessage = textMessage.toLowerCase();
    source = responseBot.getSource();
    senderId = source.getSenderId();
    replyToken = messageEvent.getReplyToken();
    profileUser = responseBot.getProfile(senderId);


    if (checkNullCurrentPath()) {
      handleNullMessage();

    } else {
      try {
        ResponseEntity<String> response = postUserData();
        handleNotNullMessage(response);
      } catch (Exception e) {
        currentPath.put(senderId, null);
        responseBot.replyText(replyToken,
            "Maaf, fitur ini sedang tidak aktif. Harap kembali ke menu.");
        e.printStackTrace();
      }
    }
  }

  /** handling Null in currentHandler. **/
  public void handleNullMessage() {
    switch (userMessage) {
      case "menu":
        responseBot.reply(replyToken, flexBot.createFlexMenu());
        break;

      case "catat pendapatan":
        currentPath.put(senderId, "record");
        responseBot.reply(replyToken, flexBot.createFlexCatatPendapatan());
        break;

      case "catat pengeluaran":
        currentPath.put(senderId, "expense");
        responseBot.reply(replyToken, flexBot.createFlexCatatPengeluaran());
        break;

      case "laporan":
        currentPath.put(senderId, "report");
        responseBot.reply(replyToken, flexBot.createFlexLaporan());
        break;

      case "kalkulator investasi":
        currentPath.put(senderId, "investment");
        responseBot.replyText(replyToken, "Placeholder message");
        break;

      default:
        String unknown = "Permintaan tidak dikenali. "
            + "Ketik 'menu' untuk melihat daftar tindakan yang dapat dilakukan.";
        responseBot.replyText(replyToken, unknown);
    }
  }

  /** if not null senderId in currentPath. **/
  public void handleNotNullMessage(ResponseEntity<String> response) {
    if (response.getStatusCode() == HttpStatus.OK) {
      String replyMessage = response.getBody();
      if (replyMessage.contains("berhasil dicatat") || replyMessage.contains("dibatalkan")) {
        currentPath.put(senderId, null);
      }
      responseBot.replyText(replyToken, response.getBody());

    } else {
      System.out.println("not OK");
      currentPath.put(senderId, null);
      responseBot.replyText(replyToken,
          "Maaf, fitur ini sedang tidak aktif. Harap kembali ke menu.");
    }
  }

  /** Send user's data. **/
  public ResponseEntity<String> postUserData() {
    Map<String, String> map = new HashMap<>();
    map.put("senderId", senderId);
    map.put("username", profileUser.getDisplayName());
    map.put("message", userMessage);

    RestTemplate restTemplate = new RestTemplate();
    ResponseEntity<String> response = restTemplate
        .postForEntity(url + currentPath.get(senderId), map, String.class);
    return response;
  }

  /** check senderId in currentPath. **/
  public boolean checkNullCurrentPath() {
    Set<String> keySet = currentPath.keySet();
    if (!keySet.contains(senderId)) {
      currentPath.put(senderId, null);
    }

    if (currentPath.get(senderId) == null) {
      return true;
    }
    return false;
  }
}
