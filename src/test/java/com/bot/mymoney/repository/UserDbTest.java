package com.bot.mymoney.repository;

import com.bot.mymoney.database.UserDao;
import com.bot.mymoney.model.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserDbTest {
    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserDb userDb;

    @Test
    void registerUserTest() {
        when(userDao.registerUser("userId", "luthfi"))
                .thenReturn(1);
        userDb.registerUser("userId", "luthfi");
        verify(userDao, times(1)).registerUser("userId", "luthfi");
    }

    @Test
    void getUserByIdTest() {
        List<User> users = new ArrayList<>();
        users.add(new User("userId", "luthfi", "false"));
        when(userDao.getByUserId("%userId%"))
                .thenReturn(users);
        userDb.getUserById("userId");
        verify(userDao, times(1)).getByUserId("%userId%");
    }

    @Test
    void getAllUsersTest() {
        List<User> users = new ArrayList<>();
        users.add(new User("userId", "luthfi", "false"));
        when(userDao.get()).thenReturn(users);
        userDb.getAllUsers();
        verify(userDao, times(1)).get();
    }

    @Test
    void getStatusNotifikasibyUserIdTest() {
        List<User> users = new ArrayList<>();
        users.add(new User("userId", "luthfi", "false"));
        when(userDao.getStatusNotifikasiByUserId("userId")).thenReturn(users);
        when(userDao.setStatusNotifikasiByUserId("status", "userId"))
                .thenReturn(1);
        userDb.setStatusNotifikasi("status", "userId");
        userDb.getStatusNotifikasiByUserId("userId");
        verify(userDao, times(1)).getStatusNotifikasiByUserId("userId");
    }

    @Test
    void getAllUserNotifikasiAktifTest() {
        List<User> users = new ArrayList<>();
        users.add(new User("userId", "luthfi", "false"));
        when(userDao.getAllUserNotifikasiAktif()).thenReturn(users);
        userDb.getAllUserNotifikasiAktif();
        verify(userDao, times(1)).getAllUserNotifikasiAktif();
    }
    @Test
    void setStatusNotifikasiTest() {
        List<User> users = new ArrayList<>();
        users.add(new User("userId", "luthfi", "false"));
        when(userDao.getStatusNotifikasiByUserId("userId")).thenReturn(users);
        when(userDao.setStatusNotifikasiByUserId("status", "userId"))
                .thenReturn(1);
        userDb.setStatusNotifikasi("status", "userId");
        userDb.getStatusNotifikasiByUserId("userId");
        verify(userDao, times(1)).getStatusNotifikasiByUserId("userId");
    }
}
