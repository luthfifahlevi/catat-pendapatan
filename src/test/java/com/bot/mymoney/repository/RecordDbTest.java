package com.bot.mymoney.repository;

import com.bot.mymoney.database.RecordDao;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RecordDbTest {
    @Mock
    private RecordDao recordDao;

    @InjectMocks
    private RecordDb recordDb;

    @Test
    void saveRecordTest() {
        String description = "userId;luthfi;Usaha;10000";
        recordDb.saveRecord(description);
        when(recordDao.saveRecord("userId", "luthfi",
                "Usaha", "timestamp", "10000"))
                .thenReturn(1);

        recordDao.saveRecord("userId", "luthfi",
                "Usaha", "timestamp", "10000");
        verify(recordDao).saveRecord("userId", "luthfi",
                "Usaha", "timestamp", "10000");
    }

    @Test
    void getByUserIdTest(){
        when(recordDao.getByUserId("userId")).thenReturn(new ArrayList<>());
        Assert.assertTrue(recordDb.getByUserId("userId") instanceof ArrayList);
    }
}
