package com.bot.mymoney.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class EventsTest {
    @InjectMocks
    Events events;

    @Test
    void getEvents() {
        assertTrue(events.getEvents() instanceof List);
    }
}
