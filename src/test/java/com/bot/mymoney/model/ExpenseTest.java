package com.bot.mymoney.model;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ExpenseTest {

    private Expense expense;

    @BeforeEach
    void setup() throws Exception {
        expense = new Expense(
                "userid",
                "name",
                "category",
                "timestamp",
                "10000"
        );
    }

    @Test
    void testExpenseGetUserId() {
        Assert.assertEquals("userid", expense.getUserId());
    }

    @Test
    void testExpenseSetUserId() {
        Assert.assertEquals("userid", expense.getUserId());
        expense.setUserId("newuserid");
        Assert.assertEquals("newuserid", expense.getUserId());
    }

    @Test
    void testExpenseGetTitle() {
        Assert.assertEquals("name", expense.getName());
    }

    @Test
    void testExpenseSetTitle() {
        Assert.assertEquals("name", expense.getName());
        expense.setName("newname");
        Assert.assertEquals("newname", expense.getName());
    }

    @Test
    void testExpenseGetCategory() {
        Assert.assertEquals("category", expense.getCategory());
    }

    @Test
    void testExpenseSetCategory() {
        Assert.assertEquals("category", expense.getCategory());
        expense.setCategory("newcategory");
        Assert.assertEquals("newcategory", expense.getCategory());
    }

    @Test
    void testExpenseGetTimestamp() {
        Assert.assertEquals("timestamp", expense.getTimestamp());
    }

    @Test
    void testExpenseSetTimestamp() {
        Assert.assertEquals("timestamp", expense.getTimestamp());
        expense.setTimestamp("newtimestamp");
        Assert.assertEquals("newtimestamp", expense.getTimestamp());
    }

    @Test
    void testExpenseGetNominal() {
        Assert.assertEquals("10000", expense.getNominal());
    }

    @Test
    void testExpenseSetNominal() {
        Assert.assertEquals("10000", expense.getNominal());
        expense.setNominal("20000");
        Assert.assertEquals("20000", expense.getNominal());
    }
}
