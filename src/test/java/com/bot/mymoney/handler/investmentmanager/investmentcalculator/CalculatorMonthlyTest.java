package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorMonthlyTest {

    private InvestmentCalculator currentState;

    @BeforeEach
    void setup() {
        currentState = new CalculatorMonthlyStart();
    }

    @Test
    void testResponseCorrect() {
        ResponseTemplate nextState = currentState.verificationMessage("ya");
        Assertions.assertTrue(nextState instanceof IcFunds);
    }

    @Test
    void testResponseCancel() {
        ResponseTemplate nextState = currentState.verificationMessage("batal");
        Assertions.assertNull(nextState);
    }

    //TODO Replace With Real test
    @Test
    void testCalculateInvestment() {
        Assertions.assertTrue(currentState.getInvestmentGrowth() != null);
    }
}
