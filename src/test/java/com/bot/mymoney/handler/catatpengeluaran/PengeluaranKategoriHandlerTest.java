package com.bot.mymoney.handler.catatpengeluaran;

import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PengeluaranKategoriHandlerTest {

    private PengeluaranKategoriHandler currentState;

    @BeforeEach
    void setup() {
        currentState = new PengeluaranKategoriHandler("userid", "username");
    }

    @Test
    void testChooseCategory() {
        ResponseTemplate nextState = currentState.verificationMessage("konsumsi");
        Assert.assertTrue(nextState instanceof PengeluaranNominalHandler);
    }

    @Test
    void testChooseWrongCategory() {
        ResponseTemplate nextState = currentState.verificationMessage("mantap");
        Assert.assertTrue(nextState instanceof PengeluaranKategoriHandler);
    }

    @Test
    void testCancelOperation() {
        ResponseTemplate nextState = currentState.verificationMessage("batal");
        Assert.assertTrue(nextState == null);
    }

    @Test
    void testGetDescription() {
        Assert.assertEquals("userid;username", currentState.getDescription());
    }
}
