package com.bot.mymoney.handler.catatpengeluaran;

import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PengeluaranNominalHandlerTest {

    private PengeluaranNominalHandler currentState;

    @BeforeEach
    void setup() {
        PengeluaranKategoriHandler oldState = new PengeluaranKategoriHandler("userid", "username");
        currentState = (PengeluaranNominalHandler) oldState.verificationMessage("konsumsi");
    }

    @Test
    void testInputValidNominal() {
        ResponseTemplate nextState = currentState.verificationMessage("10000");
        Assert.assertTrue(nextState instanceof PengeluaranKonfirmasiHandler);
    }

    @Test
    void testInputNotNumericalNominal() {
        ResponseTemplate nextState = currentState.verificationMessage("mantap");
        Assert.assertTrue(nextState instanceof PengeluaranNominalHandler);
    }

    @Test
    void testInputBelowZeroNominal() {
        ResponseTemplate nextState = currentState.verificationMessage("-5");
        Assert.assertTrue(nextState instanceof PengeluaranNominalHandler);
    }

    @Test
    void testCancelOperation() {
        ResponseTemplate nextState = currentState.verificationMessage("batal");
        Assert.assertTrue(nextState == null);
    }

    @Test
    void testGetDescription() {
        Assert.assertEquals("userid;username;konsumsi;", currentState.getDescription());
        PengeluaranKonfirmasiHandler nextState = (PengeluaranKonfirmasiHandler) currentState.verificationMessage("10000");
        Assert.assertEquals("userid;username;konsumsi;10000", nextState.getDescription());
    }
}
