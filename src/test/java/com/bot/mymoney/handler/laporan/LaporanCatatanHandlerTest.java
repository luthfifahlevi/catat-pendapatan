package com.bot.mymoney.handler.laporan;

import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LaporanCatatanHandlerTest {
  private LaporanCatatanHandler currentState;

  @BeforeEach
  void setup() {
    currentState = new LaporanCatatanHandler("userid", "Anan");
  }

  @Test
  void testChooseCategory() {
    ResponseTemplate nextState = currentState.verificationMessage("laporan pengeluaran");
    Assert.assertTrue(nextState instanceof LaporanWaktuHandler);
  }

  @Test
  void testChooseWrongCategory() {
    ResponseTemplate nextState = currentState.verificationMessage("Intentional wrong input");
    Assert.assertTrue(nextState instanceof LaporanCatatanHandler);
  }

  @Test
  void testCancelOperation() {
    ResponseTemplate nextState = currentState.verificationMessage("batal");
    Assert.assertTrue(nextState == null);
  }

  @Test
  void testGetDescription() {
    Assert.assertEquals("userid;Anan", currentState.getDescription());
  }
}

