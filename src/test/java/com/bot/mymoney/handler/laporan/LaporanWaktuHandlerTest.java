package com.bot.mymoney.handler.laporan;

import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LaporanWaktuHandlerTest {
  private LaporanWaktuHandler currentState;

  @BeforeEach
  void setup() {
    LaporanCatatanHandler oldState = new LaporanCatatanHandler("userid", "Anan");
    currentState = (LaporanWaktuHandler) oldState.verificationMessage("laporan pengeluaran");
  }

  @Test
  void testChooseCategory() {
    ResponseTemplate nextState = currentState.verificationMessage("harian");
    Assert.assertTrue(nextState == null);
  }

  @Test
  void testChooseWrongCategory() {
    ResponseTemplate nextState = currentState.verificationMessage("Intentional wrong input");
    Assert.assertTrue(nextState instanceof LaporanWaktuHandler);
  }

  @Test
  void testCancelOperation() {
    ResponseTemplate nextState = currentState.verificationMessage("batal");
    Assert.assertTrue(nextState == null);
  }

  @Test
  void testGetDescription() {
    Assert.assertEquals("userid;Anan;laporan pengeluaran", currentState.getDescription());
  }
}
