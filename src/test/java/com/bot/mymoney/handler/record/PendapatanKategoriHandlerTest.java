package com.bot.mymoney.handler.record;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PendapatanKategoriHandlerTest {
  PendapatanHandler currentHandler= new PendapatanKategoriHandler("senderId", "name");

  @Test
  void verificationMessageTrueTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("gaji");
    assertTrue(nextHandler instanceof PendapatanNominalHandler);
  }

  @Test
  void verificationMessageBatalTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("batal");
    assertEquals(nextHandler, null);
  }

  @Test
  void verificationMessageUnknownTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("hai");
    assertTrue(nextHandler instanceof PendapatanKategoriHandler);
  }

  @Test
  void getDescription() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("gaji");
    String description = currentHandler.getDescription();
    assertEquals("senderId;name;gaji", description);
  }
}
