package com.bot.mymoney.handler.record;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PendapatanKonfirmasiHandlerTest {
  PendapatanHandler currentHandler= new PendapatanKonfirmasiHandler(
      new PendapatanNominalHandler(
          new PendapatanKategoriHandler("senderId", "name"))
  );

  @Test
  void verificationMessageTrueTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("ya");
    assertEquals(nextHandler, null);
  }

  @Test
  void verificationMessageBatalTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("batal");
    assertEquals(nextHandler, null);
  }

  @Test
  void verificationMessageUnknownTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("hai");
    assertTrue(nextHandler instanceof PendapatanKonfirmasiHandler);
  }

  @Test
  void getDescription() {
    String description = currentHandler.getDescription();
    assertEquals("senderId;name;null;null", description);
  }
}
