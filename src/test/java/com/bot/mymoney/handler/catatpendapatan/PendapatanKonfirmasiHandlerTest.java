package com.bot.mymoney.handler.catatpendapatan;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PendapatanKonfirmasiHandlerTest {
  Handler currentHandler= new PendapatanKonfirmasiHandler(
      new PendapatanNominalHandler(
          new PendapatanKategoriHandler("senderId", "name"))
  );

  @Test
  void verificationMessageTrueTest() {
    ResponseTemplate nextHandler = this.currentHandler.verificationMessage("ya");
    assertEquals(nextHandler, null);
  }

  @Test
  void verificationMessageBatalTest() {
    ResponseTemplate nextHandler = this.currentHandler.verificationMessage("batal");
    assertEquals(nextHandler, null);
  }

  @Test
  void verificationMessageUnknownTest() {
    ResponseTemplate nextHandler = this.currentHandler.verificationMessage("hai");
    assertTrue(nextHandler instanceof PendapatanKonfirmasiHandler);
  }

  @Test
  void getDescription() {
    String description = currentHandler.getDescription();
    assertEquals("senderId;name;null;null", description);
  }
}
