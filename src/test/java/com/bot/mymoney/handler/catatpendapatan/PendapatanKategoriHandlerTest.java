package com.bot.mymoney.handler.catatpendapatan;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PendapatanKategoriHandlerTest {
  Handler currentHandler= new PendapatanKategoriHandler("senderId", "name");

  @Test
  void verificationMessageTrueTest() {
    ResponseTemplate nextHandler = this.currentHandler.verificationMessage("gaji");
    assertTrue(nextHandler instanceof PendapatanNominalHandler);
  }

  @Test
  void verificationMessageBatalTest() {
    ResponseTemplate nextHandler = this.currentHandler.verificationMessage("batal");
    assertEquals(nextHandler, null);
  }

  @Test
  void verificationMessageUnknownTest() {
    ResponseTemplate nextHandler = this.currentHandler.verificationMessage("hai");
    assertTrue(nextHandler instanceof PendapatanKategoriHandler);
  }

  @Test
  void getDescription() {
    ResponseTemplate nextHandler = this.currentHandler.verificationMessage("gaji");
    String description = currentHandler.getDescription();
    assertEquals("senderId;name;gaji", description);
  }
}
