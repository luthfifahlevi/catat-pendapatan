package com.bot.mymoney.handler.expense;

import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PengeluaranNominalHandlerTest {

    private PengeluaranNominalHandler currentState;

    @BeforeEach
    void setup() {
        PengeluaranKategoriHandler oldState = new PengeluaranKategoriHandler("userid", "username");
        currentState = (PengeluaranNominalHandler) oldState.verificationMessage("konsumsi");
    }

    @Test
    void testInputValidNominal() {
        PengeluaranHandler nextState = currentState.verificationMessage("10000");
        Assert.assertTrue(nextState instanceof PengeluaranKonfirmasiHandler);
    }

    @Test
    void testInputNotNumericalNominal() {
        PengeluaranHandler nextState = currentState.verificationMessage("mantap");
        Assert.assertTrue(nextState instanceof PengeluaranNominalHandler);
    }

    @Test
    void testInputBelowZeroNominal() {
        PengeluaranHandler nextState = currentState.verificationMessage("-5");
        Assert.assertTrue(nextState instanceof PengeluaranNominalHandler);
    }

    @Test
    void testCancelOperation() {
        PengeluaranHandler nextState = currentState.verificationMessage("batal");
        Assert.assertTrue(nextState == null);
    }

    @Test
    void testGetDescription() {
        Assert.assertEquals("userid;username;konsumsi;", currentState.getDescription());
        PengeluaranKonfirmasiHandler nextState = (PengeluaranKonfirmasiHandler) currentState.verificationMessage("10000");
        Assert.assertEquals("userid;username;konsumsi;10000", nextState.getDescription());
    }
}
