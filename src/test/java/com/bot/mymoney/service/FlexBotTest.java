package com.bot.mymoney.service;

import com.linecorp.bot.model.message.FlexMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlexBotTest {
  @InjectMocks
  private FlexBotImp flexBot;

  @Mock
  private ResponseBot responseBot;

  @Mock
  private HandlingBot handlingBot;

  @Test
  void createFlexCatatPendapatanTest() {
    FlexMessage flexMessage = flexBot.createFlexCatatPendapatan();
    assertNotEquals(flexMessage, null);
  }

  @Test
  void createFlexCatatPengeluaranTest() {
    FlexMessage flexMessage = flexBot.createFlexCatatPengeluaran();
    assertNotEquals(flexMessage, null);
  }

  @Test
  void createFlexMenu() {
    FlexMessage flexMessage = flexBot.createFlexMenu();
    assertNotEquals(flexMessage, null);
  }

  @Test
  void createFlexLaporan() {
    FlexMessage flexMessage = flexBot.createFlexLaporan();
    assertNotEquals(flexMessage, null);
  }

  @Test
  void createFlexLaporanWaktu() {
    FlexMessage flexMessage = flexBot.createFlexLaporanWaktu();
    assertNotEquals(flexMessage, null);
  }
}