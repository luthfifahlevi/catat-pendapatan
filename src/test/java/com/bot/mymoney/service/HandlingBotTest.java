package com.bot.mymoney.service;

import com.linecorp.bot.model.event.MessageEvent;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class HandlingBotTest {
  private Class<?> handlingBotServiceClass;

  @InjectMocks
  HandlingBotImp handlingBot = new HandlingBotImp();

  @BeforeEach
  public void setup() throws Exception {
    handlingBotServiceClass = Class.forName("com.bot.mymoney.service.HandlingBotImp");
  }

  @Test
  public void testHandlingBotHasHandleMessageEventMethod() throws Exception {
    Method handleMessageEvent = handlingBotServiceClass
        .getDeclaredMethod("handleMessageEvent", MessageEvent.class);
    int methodModifiers = handleMessageEvent.getModifiers();

    Assert.assertTrue(Modifier.isPublic(methodModifiers));
    Assert.assertEquals(1, handleMessageEvent.getParameterCount());
    Assert.assertEquals("void", handleMessageEvent.getGenericReturnType().getTypeName());
  }

  @Test
  public void testHandlingBotHasHandleNullMessageMethod() throws Exception {
    Method handleNullMessage = handlingBotServiceClass
        .getDeclaredMethod("handleNullMessage");
    int methodModifiers = handleNullMessage.getModifiers();

    Assert.assertTrue(Modifier.isPublic(methodModifiers));
    Assert.assertEquals(0, handleNullMessage.getParameterCount());
    Assert.assertEquals("void", handleNullMessage.getGenericReturnType().getTypeName());
  }
}
