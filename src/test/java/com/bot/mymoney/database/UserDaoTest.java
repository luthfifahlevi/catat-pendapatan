package com.bot.mymoney.database;

import com.bot.mymoney.model.User;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserDaoTest {
    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private ResultSet resultSet;

    @InjectMocks
    UserDaoImp userDao = new UserDaoImp(getDataSource());

    DataSource getDataSource() {
        String dbUrl = System.getenv("JDBC_DATABASE_URL");
        String username = System.getenv("JDBC_DATABASE_USERNAME");
        String password = System.getenv("JDBC_DATABASE_PASSWORD");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }

    @Test
    void getTest() {
        List<User> users = userDao.get();
        Assert.assertNull(users);
    }

    @Test
    void getByUserIdTest() {
        List<User> users = userDao.getByUserId("userId");
        Assert.assertNull(users);
    }

    @Test
    void registerUserTest() {
        int user = userDao.registerUser("userId", "displayName");
        Assert.assertEquals(user, 0);
    }

    @Test
    void statusNotifikasiByUserIdTest() {
        userDao.setStatusNotifikasiByUserId("true", "userId");
        List<User> userList = userDao.getStatusNotifikasiByUserId("userId");
        Assert.assertNull(userList);
        userList = userDao.getAllUserNotifikasiAktif();
        Assert.assertNull(userList);
    }

    @Test
    void extractDataTest() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        List<User> users = userDao.extractData(resultSet);
        Assert.assertNotNull(users);
    }
}
