package com.bot.mymoney.database;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.sql.ResultSet;

public class ExpenseDaoTest {
    private Class<?> expenseDaoClass;

    @BeforeEach
    void setup() throws Exception {
        expenseDaoClass = Class.forName("com.bot.mymoney.database.ExpenseDaoImpl");
    }

    @Test
    void testExpenseDaoHasExtractDataMethod() throws Exception {
        Method extractData = expenseDaoClass.getDeclaredMethod("extractData", ResultSet.class);
        int methodModifiers = extractData.getModifiers();

        Assert.assertTrue(Modifier.isPublic(methodModifiers));
        Assert.assertTrue(Modifier.isStatic(methodModifiers));
        Assert.assertEquals("java.util.List<com.bot.mymoney.model.Expense>",
                extractData.getGenericReturnType().getTypeName());
    }

    @Test
    void testExpenseDaoHasGetMethod() throws Exception {
        Method get = expenseDaoClass.getDeclaredMethod("get");
        int methodModifiers = get.getModifiers();

        Assert.assertTrue(Modifier.isPublic(methodModifiers));
        Assert.assertEquals("java.util.List<com.bot.mymoney.model.Expense>",
                get.getGenericReturnType().getTypeName());
    }

    @Test
    void testExpenseDaoHasGetByUserIdMethod() throws Exception {
        Method getByUserId = expenseDaoClass.getDeclaredMethod("getByUserId", String.class);
        int methodModifiers = getByUserId.getModifiers();

        Assert.assertTrue(Modifier.isPublic(methodModifiers));
        Assert.assertEquals("java.util.List<com.bot.mymoney.model.Expense>",
                getByUserId.getGenericReturnType().getTypeName());
    }

    @Test
    void testExpenseDaoHasSaveExpenseMethod() throws Exception {
        Method saveExpense = expenseDaoClass.getDeclaredMethod("saveExpense"
                , String.class, String.class, String.class, String.class, String.class);
        int methodModifiers = saveExpense.getModifiers();

        Assert.assertTrue(Modifier.isPublic(methodModifiers));
        Assert.assertEquals("int",
                saveExpense.getGenericReturnType().getTypeName());
    }
}
